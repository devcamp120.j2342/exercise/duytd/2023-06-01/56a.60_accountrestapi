package com.devcamp.accountestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountestapiApplication.class, args);
	}

}
