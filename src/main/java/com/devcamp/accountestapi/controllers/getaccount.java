package com.devcamp.accountestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountestapi.models.Account;

@RestController
@RequestMapping("/")
public class getaccount {
    @GetMapping("/accounts")
    public ArrayList<Account> getAccount(){
        Account account1 = new Account("1", "Nguyen Tan Hoang", 10000);
        Account account2 = new Account("2", "Truong Hoang Phương", 20000);
        Account account3 = new Account("3", "Tran Minh Duc", 30000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        ArrayList<Account> ArrListAccount = new ArrayList<Account>();

        ArrListAccount.add(account1);
        ArrListAccount.add(account2);
        ArrListAccount.add(account3);

        return ArrListAccount;
    }
}
